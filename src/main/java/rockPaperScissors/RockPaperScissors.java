package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
            System.out.println("Let's play round "+ roundCounter);
            String humanChoice = userChoice("Your choice (Rock/Paper/Scissors)?", rpsChoices);
            String compChoice = computerChoice();

            String choiceString = "Human chose " +humanChoice+ ", computer chose " +compChoice+ ".";

            if (humanChoice.equals(compChoice)) {
                System.out.println(choiceString +" It's a tie!");
                
            }
            else if (!isWinner(humanChoice,compChoice)){
                System.out.println(choiceString +" Computer wins!");
                computerScore++;
            }
            else{
                System.out.println(choiceString +" Human wins!");
                humanScore++;
            }
            System.out.println("Score: human "+humanScore+", computer "+computerScore);

            String continueAnswer = userChoice("Do you wish to continue playing? (y/n)?",Arrays.asList("y", "n"));
            if (continueAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter++;
        }
    }
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }
        else {
            return choice2.equals("paper");
        }
    }
    
    public String computerChoice() {
        Random random = new Random();
        int randomIndex = random.nextInt(rpsChoices.size());
        return rpsChoices.get(randomIndex);
    }


    public String userChoice(String prompt, List<String> validInput) {
        String userChoice;
        while(true) {
            userChoice = readInput(prompt);
            if (validateInput(userChoice, validInput)){
                break;
            }
            else {
                System.out.println("I do not understand" + userChoice + ". Could you try again?");
            }
        }
        return userChoice;
    }

    public boolean validateInput(String input, List<String> validInput) {
        return validInput.contains(input);
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
